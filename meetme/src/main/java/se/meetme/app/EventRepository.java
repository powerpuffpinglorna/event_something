package se.meetme.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;
import se.meetme.app.model.Event;

import java.util.List;

@Repository
public class EventRepository  {

    @Autowired
    private MongoTemplate mongoTemplate;

    public List<Event> getEvents() {
        return mongoTemplate.findAll(Event.class);
    }

    public void saveEvent(Event event) {
        mongoTemplate.insert(event);
    }
}
