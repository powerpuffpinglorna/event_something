package se.meetme.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se.meetme.app.EventRepository;
import se.meetme.app.model.Event;

import java.util.ArrayList;
import java.util.List;

@Service
public class EventService {

    @Autowired
    private EventRepository eventRepository;

    public List<Event> listEvents() {

        return eventRepository.getEvents();
    }

    public void insertEvent(Event event) {
        eventRepository.saveEvent(event);
    }
}
