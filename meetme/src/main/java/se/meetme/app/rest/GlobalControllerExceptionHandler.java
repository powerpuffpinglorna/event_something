package se.meetme.app.rest;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;

@ControllerAdvice
public class GlobalControllerExceptionHandler {

    private static final Logger LOG = Logger.getLogger(GlobalControllerExceptionHandler.class);


    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public String handleException(Exception ex, HttpServletResponse response) {
        LOG.error("system error occured", ex);

        response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());

        return "booom";
    }
}
